package com.daily_droid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daily_droid.data.AppOfDayHTTPClient;
import com.daily_droid.data.JsonParser;
import com.daily_droid.model.AppOfDayInfo;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONException;


public class MainActivity extends YouTubeActivity implements YouTubePlayer.OnInitializedListener {

    private ImageView icon;
    private TextView name;
    private TextView dev;
    private Button link;
    private TextView category;
    private TextView size;
    private TextView rating;
    private ImageView image;
    //private WebView video;
    private Bitmap mainIcon = null;
    private Bitmap mainImage = null;
    private static final int RECOVERY_REQUEST = 1;
    public YouTubePlayerView youTubeView;
    public String videoUrl = "lGkMn0Z8Xz0";
    private volatile boolean valueVideoFlag = false;
    YouTubePlayer videoPlayer;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!(isNetworkAvailable(getApplicationContext()))) {

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Connexion");
            alertDialog.setMessage("Désolé, votre connexion Internet ne fonctionne pas, merci de l'activer pour utiliser Daily Droid.");
            alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    System.exit(0);
                }
            });

            alertDialog.show();

        } // end if

        icon = (ImageView) findViewById(R.id.icon);
        name = (TextView) findViewById(R.id.name);
        dev = (TextView) findViewById(R.id.dev);
        link = (Button) findViewById(R.id.link);
        category = (TextView) findViewById(R.id.category);
        size = (TextView) findViewById(R.id.size);
        rating = (TextView) findViewById(R.id.rating);
        image = (ImageView) findViewById(R.id.image);
        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(YouTubeApiKey.YOUTUBE_API_KEY, this);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    } // end onCreate

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {

            videoPlayer = player;
            JSONAppOfDayTask task = new JSONAppOfDayTask();
            task.execute(new String[]{});
             // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = getString(R.string.player_error);
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(YouTubeApiKey.YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    } // end of

    public boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    } // end isNetworkAvailable

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.daily_droid/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.daily_droid/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    private class JSONAppOfDayTask extends AsyncTask<String, Void, AppOfDayInfo> {

        @Override
        protected AppOfDayInfo doInBackground(String... params) {
            AppOfDayInfo appOfDayInfo = new AppOfDayInfo();

            String data = ((new AppOfDayHTTPClient()).getAppOfDayData());

            Log.i("MainActivity", "URL : " + data);

            try {
                appOfDayInfo = JsonParser.getAppOfDayInfo(data);

                // Let's retrieve the icon and the main image
                mainIcon = ((new AppOfDayHTTPClient()).getImage(appOfDayInfo.getIcon()));
                mainImage = ((new AppOfDayHTTPClient()).getImage(appOfDayInfo.getImage()));

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return appOfDayInfo;

        } // end doInBackground

        @Override
        protected void onPostExecute(final AppOfDayInfo appOfDayInfo) {
            //super.onPostExecute(appOfDayInfo);
            if (mainIcon != null) {
                icon.setImageBitmap(mainIcon);
            } // and if for icon

            name.setText(appOfDayInfo.getName());
            dev.setText(appOfDayInfo.getDev());
            link.findViewById(R.id.link).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = appOfDayInfo.getLink();

                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });

            category.setText(appOfDayInfo.getCategory());
            size.setText(appOfDayInfo.getSize());
            rating.setText(appOfDayInfo.getRating());
            if (mainImage != null) {
                image.setImageBitmap(mainImage);
            } // and if for image

            Log.i("MainActivity", "YouTube Ref. : " + appOfDayInfo.getVideo());
            /*
            video.getSettings().setJavaScriptEnabled(true);
            video.getSettings().setPluginState(WebSettings.PluginState.ON);
            video.loadUrl(appOfDayInfo.getVideo());
            video.setWebChromeClient(new WebChromeClient());
            */
            videoPlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
            videoPlayer.cueVideo(appOfDayInfo.getVideo());

        } // end onPostExecute

    } // end JSONAppOfDayTask

} // end MainActivity class
