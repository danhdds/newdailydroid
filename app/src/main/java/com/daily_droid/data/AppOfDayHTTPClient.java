package com.daily_droid.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Danilo on 31/08/2015.
 */
public class AppOfDayHTTPClient {

    private static String BASE_URL = "http://freelanceando.net/dailydroid/webscrapp/appOfDay.php";

    public String getAppOfDayData(){

        HttpURLConnection con = null;
        InputStream is = null;



        try {
            // set the connection
            con = (HttpURLConnection) ( new URL(BASE_URL )).openConnection();
            Log.i("MainActivity", "URL : " + BASE_URL);
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // read response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ( ( line = br.readLine() ) != null ){
                buffer.append( line + "\r\n" );
            }

            is.close();
            con.disconnect();

            return buffer.toString();

        }catch (Throwable t){

            t.printStackTrace();

        }finally {

            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}

        } // end try catch finally

        return null;


    } //end getAppOfDay

    public Bitmap getImage(String imgUrl) {
        HttpURLConnection con = null ;
        InputStream is = null;
        Bitmap bm = null;
        BufferedInputStream bis = null;
        try {
            con = (HttpURLConnection) ( new URL("http:"+imgUrl)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            is = con.getInputStream();
            bis = new BufferedInputStream(is, 8192);

            bm = BitmapFactory.decodeStream(bis);

            return bm;
        } // end try
        catch(Throwable t) {
            t.printStackTrace();
        } // end catch
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        } // end finally

        return null;

    } // end getImage

} // end AppOfDayHTTPClient
