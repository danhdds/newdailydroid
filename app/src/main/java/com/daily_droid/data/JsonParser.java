package com.daily_droid.data;

import com.daily_droid.model.AppOfDayInfo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Danilo on 31/08/2015.
 */
public class JsonParser {

    public static AppOfDayInfo getAppOfDayInfo(String data) throws JSONException{

        AppOfDayInfo appOfDayInfo = new AppOfDayInfo();
        JSONObject jsonObject = new JSONObject(data);

        // stract the information from the object
        appOfDayInfo.setIcon(getString("icon", jsonObject));
        appOfDayInfo.setName(getString("name", jsonObject));
        appOfDayInfo.setDev(getString("dev", jsonObject));
        appOfDayInfo.setLink(getString("link", jsonObject));
        appOfDayInfo.setCategory(getString("category", jsonObject));
        appOfDayInfo.setSize(getString("size", jsonObject));
        appOfDayInfo.setRating(getString("rating", jsonObject));
        appOfDayInfo.setImage(getString("image", jsonObject));
        appOfDayInfo.setVideo(getString("video", jsonObject));

        return appOfDayInfo;

    } // end AppOfDayInfo

    private static JSONObject getObject(String tagName, JSONObject jObj)  throws JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }

    private static String getString(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getString(tagName);
    }

    private static float  getFloat(String tagName, JSONObject jObj) throws JSONException {
        return (float) jObj.getDouble(tagName);
    }

    private static int  getInt(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getInt(tagName);
    }

} // end JsonParser class
